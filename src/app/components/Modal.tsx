import { useEffect, useState } from 'react'
import ReactModal from 'react-modal'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { apiClientPusat, apiClientCabang } from '../utils/api-client'

const modalStyles = {
  content: 'bg-white p-8 rounded shadow-md w-full max-w-md mx-auto'
}

const overlayStyles = {
  overlay:
    'fixed inset-0 flex items-center justify-center bg-black bg-opacity-50'
}

function Modal({
  isOpen,
  isEdit,
  categories,
  taxGroups,
  unitsOfMeasure,
  handleRevalidate,
  formEdit,
  onClose,
  closeModal,
  backToFirstPage
}: any) {
  const [isLoading, setIsLoading] = useState<boolean>(false)

  const [formData, setFormData] = useState<any>({
    number: '',
    displayName: '',
    type: '',
    blocked: false,
    baseUnitOfMeasure: {
      code: '',
      displayName: '',
      symbol: '',
      unitConversion: null
    },
    gtin: '',
    itemCategory: {
      categoryId: '',
      description: ''
    },
    inventory: '',
    unitPrice: '',
    priceIncludesTax: false,
    unitCost: '',
    taxGroupCode: ''
  })

  useEffect(() => {
    if (isEdit) {
      setFormData((prevFormData: any) => {
        const newFormData = { ...prevFormData }

        for (const key in formEdit) {
          if (Object.hasOwnProperty.call(formEdit, key)) {
            if (
              key !== 'lastModifiedDateTime' &&
              key !== 'baseUnitOfMeasureId' &&
              key !== 'taxGroupId' &&
              key !== 'itemCategoryId' &&
              key !== '@odata.etag' &&
              key !== 'id'
            ) {
              if (formEdit[key] !== null) {
                newFormData[key] = formEdit[key]
              }
            }
          }
        }

        return newFormData
      })
    }
  }, [isEdit, formEdit])

  const handleChange = (
    e:
      | React.ChangeEvent<HTMLInputElement>
      | React.ChangeEvent<HTMLTextAreaElement>
      | React.ChangeEvent<HTMLSelectElement>
  ) => {
    const { name, value } = e.target
    if (name === 'category' || name === 'unitOfMeasure') {
      const category = categories.find(
        (cat: any) => cat.code === value
      )?.displayName
      const unit = unitsOfMeasure.find(
        (unit: any) => unit.code === value
      )?.displayName
      setFormData({
        ...formData,
        [name === 'category' ? 'itemCategory' : 'baseUnitOfMeasure']: {
          ...(name === 'unitOfMeasure'
            ? {
                code: value,
                displayName: unit,
                symbol: '',
                unitConversion: null
              }
            : {
                categoryId: value,
                description: category
              })
        }
      })
    } else if (name === 'blocked' || name === 'priceIncludesTax') {
      setFormData({
        ...formData,
        [name]: (e.target as HTMLInputElement).checked
      })
    } else if (name === 'unitPrice') {
      const regex = /^[0-9.,]*$/
      if (regex.test(value)) {
        setFormData({
          ...formData,
          [name]: value
        })
      }
    } else {
      setFormData({
        ...formData,
        [name]: value
      })
    }
  }

  const setFormEmpty = () => {
    setFormData({
      number: '',
      displayName: '',
      type: '',
      blocked: false,
      baseUnitOfMeasure: {
        code: '',
        displayName: '',
        symbol: '',
        unitConversion: null
      },
      gtin: '',
      itemCategory: {
        categoryId: '',
        description: ''
      },
      inventory: '',
      unitPrice: '',
      priceIncludesTax: false,
      unitCost: '',
      taxGroupCode: ''
    })
  }

  const handleCloseModal = () => {
    onClose()
    setFormEmpty()
  }

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    const data = {
      ...formData,
      baseUnitOfMeasure: {
        ...formData.baseUnitOfMeasure,
        symbol: isEdit ? '' : formData.baseUnitOfMeasure.symbol
      },
      inventory: Number(formData.inventory),
      unitPrice: Number(formData.unitPrice),
      unitCost: Number(formData.unitCost)
    }

    const customHeaders = {
      'If-Match': formEdit['@odata.etag']
    }

    setIsLoading(true)
    try {
      isEdit
        ? await apiClientPusat.patch(`/items(${formEdit.id})`, data, {
            headers: customHeaders
          })
        : await Promise.allSettled([
            apiClientPusat.post('/items', data),
            apiClientCabang.post('/items', data)
          ])

      toast.success(`Item ${isEdit ? 'edited' : 'added'} successfully!`, {
        position: 'top-center',
        autoClose: 1500,
        hideProgressBar: true,
        theme: 'colored'
      })
      setIsLoading(false)
      setFormEmpty()
      closeModal()
      handleRevalidate()
      backToFirstPage()

      const res = await apiClientCabang.get(
        `/items?$filter=number eq '${formEdit.number}'`
      )
      const itemsCabang = res.data.value
      if (itemsCabang.length > 0) {
        await apiClientCabang.patch(`/items(${itemsCabang[0].id})`, data, {
          headers: {
            'If-Match': itemsCabang[0]['@odata.etag']
          }
        })
      }
    } catch (error) {
      console.error(error)
      setIsLoading(false)
    }
  }

  return (
    <ReactModal
      isOpen={isOpen}
      onRequestClose={handleCloseModal}
      className={modalStyles.content}
      overlayClassName={overlayStyles.overlay}
      ariaHideApp={false}
    >
      <div className='flex justify-between items-center mb-4'>
        <h2 className='text-2xl font-semibold'>
          {isEdit ? 'Edit' : 'New'} Item
        </h2>
        <button
          onClick={() => {
            setFormEmpty()
            closeModal()
          }}
          className='focus:outline-none'
        >
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='h-6 w-6 text-gray-700 hover:text-gray-900'
            fill='none'
            viewBox='0 0 24 24'
            stroke='currentColor'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeWidth='2'
              d='M6 18L18 6M6 6l12 12'
            />
          </svg>
        </button>
      </div>
      <form onSubmit={handleSubmit}>
        <div className='flex mb-4 gap-3 w-full'>
          <div className='w-full'>
            <label htmlFor='number' className='block text-gray-700 font-medium'>
              Number
            </label>
            <input
              type='text'
              id='number'
              name='number'
              value={formData.number}
              onChange={handleChange}
              className='form-input mt-1 block w-full border border-black rounded-md p-2'
              placeholder='1896-SS'
              required
            />
          </div>
          <div className='w-full'>
            <label
              htmlFor='displayName'
              className='block text-gray-700 font-medium'
            >
              Name
            </label>
            <input
              type='text'
              id='displayName'
              name='displayName'
              value={formData.displayName}
              onChange={handleChange}
              className='form-input mt-1 block w-full border border-black rounded-md p-2'
              placeholder='ATHENS Desk'
              required
            />
          </div>
        </div>
        <div className='flex mb-4 gap-3 w-full'>
          <div className='w-full'>
            <label htmlFor='type' className='block text-gray-700 font-medium'>
              Type
            </label>
            <input
              type='text'
              id='type'
              name='type'
              value={formData.type}
              onChange={handleChange}
              className='form-input mt-1 block w-full border border-black rounded-md p-2'
              placeholder='Inventory'
              required
            />
          </div>
          <div className='w-full'>
            <label
              htmlFor='category'
              className='block text-gray-700 font-medium'
            >
              Category
            </label>
            <select
              id='category'
              name='category'
              value={formData.itemCategory.categoryId}
              onChange={handleChange}
              className='form-select mt-1 block w-full border border-black rounded-md cursor-pointer'
              style={{ padding: '0.6rem 0' }}
              required
            >
              <option className='' value=''>
                Select Category
              </option>
              {categories.map((cat: any) => (
                <option key={cat.code} value={cat.code}>
                  {cat.displayName}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className='flex mb-4 gap-3 w-full'>
          <div className='w-full'>
            <label
              htmlFor='inventory'
              className='block text-gray-700 font-medium'
            >
              Inventory
            </label>
            <input
              type='number'
              id='inventory'
              name='inventory'
              value={formData.inventory}
              onChange={handleChange}
              className='form-input mt-1 block w-full border border-black rounded-md p-2'
              placeholder='0'
              required
            />
          </div>
          <div className='w-full'>
            <label
              htmlFor='unitPrice'
              className='block text-gray-700 font-medium'
            >
              Unit Price
            </label>
            <input
              type='text'
              id='unitPrice'
              name='unitPrice'
              value={formData.unitPrice}
              onChange={handleChange}
              className='form-input mt-1 block w-full border border-black rounded-md p-2'
              placeholder='1000.8'
              required
            />
          </div>
        </div>
        <div className='flex mb-4 gap-3 w-full'>
          <div className='w-full'>
            <label
              htmlFor='unitCost'
              className='block text-gray-700 font-medium'
            >
              Unit Cost
            </label>
            <input
              type='number'
              id='unitCost'
              name='unitCost'
              value={formData.unitCost}
              onChange={handleChange}
              className='form-input mt-1 block w-full border border-black rounded-md p-2'
              placeholder='0'
              required
            />
          </div>
          <div className='w-full'>
            <label
              htmlFor='unitOfMeasure'
              className='block text-gray-700 font-medium'
            >
              Unit of Measure
            </label>
            <select
              id='unitOfMeasure'
              name='unitOfMeasure'
              value={formData.baseUnitOfMeasure.code}
              onChange={handleChange}
              className='form-select mt-1 block w-full border border-black rounded-md cursor-pointer'
              style={{ padding: '0.6rem 0' }}
              required
            >
              <option className='' value=''>
                Select Unit of Measure
              </option>
              {unitsOfMeasure.map((unit: any) => (
                <option key={unit.code} value={unit.code}>
                  {unit.displayName}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className='flex mb-4 gap-3 w-full'>
          <div className='w-full'>
            <label
              htmlFor='taxGroupCode'
              className='block text-gray-700 font-medium'
            >
              Tax Group
            </label>
            <select
              id='taxGroupCode'
              name='taxGroupCode'
              value={formData.taxGroupCode}
              onChange={handleChange}
              className='form-select mt-1 block w-full border border-black rounded-md cursor-pointer'
              style={{ padding: '0.6rem 0' }}
              required
            >
              <option className='' value=''>
                Select Tax Group
              </option>
              {taxGroups.map((tax: any) => (
                <option key={tax.code} value={tax.code}>
                  {tax.displayName}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className='flex mb-[1.5rem] gap-3 w-full'>
          <div className='flex items-center w-50'>
            <input
              type='checkbox'
              id='blocked'
              name='blocked'
              checked={formData.blocked}
              onChange={handleChange}
              className='form-checkbox mr-2 cursor-pointer'
            />
            <label
              htmlFor='blocked'
              className=' text-gray-700 font-medium cursor-pointer'
            >
              Blocked
            </label>
          </div>
          <div className='flex items-center w-50 ml-5'>
            <input
              type='checkbox'
              id='priceIncludesTax'
              name='priceIncludesTax'
              checked={formData.priceIncludesTax}
              onChange={handleChange}
              className='form-checkbox mr-2 cursor-pointer'
            />
            <label
              htmlFor='priceIncludesTax'
              className='text-gray-700 font-medium cursor-pointer'
            >
              Price Includes Tax
            </label>
          </div>
        </div>
        <div>
          <button
            type='submit'
            className='bg-blue-500 hover:bg-blue-600 text-white font-medium py-2 px-4 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-400 w-full'
          >
            {isLoading ? 'Loading...' : 'Submit'}
          </button>
        </div>
      </form>
      <ToastContainer />
    </ReactModal>
  )
}

export default Modal
