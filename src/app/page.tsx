'use client'
import { useState, useEffect } from 'react'
import Image from 'next/image'
import useSWR, { mutate } from 'swr'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Modal from './components/Modal'
import Edit from './assets/icons/edit.svg'
import Delete from './assets/icons/delete.svg'
import { apiClientCabang, apiClientPusat } from './utils/api-client'

const fetcher = (url: string) => apiClientPusat.get(url).then((res) => res.data)

export default function Home() {
  const [items, setItems] = useState<any>([])
  const [categories, setCategories] = useState<any[]>([])
  const [taxGroups, setTaxGroups] = useState<any[]>([])
  const [unitsOfMeasure, setUnitsOfMeasure] = useState<any[]>([])
  const [displayedItems, setDisplayedItems] = useState<any>([])
  const [currentPage, setCurrentPage] = useState<number>(1)
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false)
  const [isEdit, setIsEdit] = useState<boolean>(false)
  const [formEdit, setFormEdit] = useState<any>({})

  const urls = ['/items', '/itemCategories', '/taxGroups', '/unitsOfMeasure']
  const {
    data: itemsData,
    error: itemsError,
    isLoading
  } = useSWR(urls[0], fetcher)
  const { data: categoriesData, error: categoriesError } = useSWR(
    urls[1],
    fetcher
  )
  const { data: taxGroupsData, error: taxGroupsError } = useSWR(
    urls[2],
    fetcher
  )
  const { data: unitsOfMeasureData, error: unitsOfMeasureError } = useSWR(
    urls[3],
    fetcher
  )

  useEffect(() => {
    if (itemsData) {
      setItems(itemsData?.value)
      setDisplayedItems(itemsData?.value.slice(0, 10))
    }

    if (categoriesData) setCategories(categoriesData?.value)
    if (taxGroupsData) setTaxGroups(taxGroupsData?.value)
    if (unitsOfMeasureData) setUnitsOfMeasure(unitsOfMeasureData?.value)

    if (itemsError || categoriesError || taxGroupsError || unitsOfMeasureError)
      console.log(
        itemsError,
        categoriesError,
        taxGroupsError,
        unitsOfMeasureError
      )
  }, [
    itemsData,
    categoriesData,
    taxGroupsData,
    unitsOfMeasureData,
    itemsError,
    categoriesError,
    taxGroupsError,
    unitsOfMeasureError
  ])

  const totalItems = items.length
  const itemsPerPage = 10

  const totalPages = Math.ceil(totalItems / itemsPerPage)

  const maxVisiblePages = 5

  const getPageButtons = () => {
    const pageButtons = []
    const totalPages = Math.ceil(totalItems / itemsPerPage)

    let startPage = Math.max(1, currentPage - Math.floor(maxVisiblePages / 2))
    let endPage = Math.min(totalPages, startPage + maxVisiblePages - 1)

    if (endPage - startPage + 1 < maxVisiblePages) {
      startPage = Math.max(1, endPage - maxVisiblePages + 1)
    }

    if (startPage > 1) {
      pageButtons.push(
        <span
          className='text-gray-700 font-medium py-2 px-4'
          key='ellipsis-start'
        >
          ...
        </span>
      )
    }

    for (let i = startPage; i <= endPage; i++) {
      const isActive = i === currentPage
      pageButtons.push(
        <button
          key={i}
          className={
            isActive
              ? 'bg-blue-500 hover:bg-blue-600 text-white font-medium py-2 px-4 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-400'
              : 'bg-white hover:bg-gray-50 text-gray-700 font-medium py-2 px-4 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-400'
          }
          onClick={() => handlePageClick(i)}
        >
          {i}
        </button>
      )
    }

    if (endPage < totalPages) {
      pageButtons.push(
        <span
          className='text-gray-700 font-medium py-2 px-4'
          key='ellipsis-end'
        >
          ...
        </span>
      )
    }

    return pageButtons
  }

  const handleRevalidate = () => {
    mutate(urls[0])
  }

  const handleEdit = (item: any) => {
    setIsModalOpen(true)
    setIsEdit(true)
    setFormEdit(item)
  }

  const handleDelete = async (item: any) => {
    const customHeaders = {
      'If-Match': '*'
    }

    const confirm = window.confirm('Are you sure you want to delete?')
    if (confirm) {
      try {
        await apiClientPusat.delete(`/items(${item.id})`, {
          headers: customHeaders
        })
        await apiClientPusat.get('/items').then((res) => {
          setItems(res.data.value)
          setDisplayedItems(res.data.value.slice(0, 10))
          toast.success('Item deleted successfully!', {
            position: 'top-center',
            autoClose: 1500,
            hideProgressBar: true,
            theme: 'colored'
          })
        })
        const res = await apiClientCabang.get(
          `/items?$filter=number eq '${item.number}'`
        )
        const itemsCabang = res.data.value
        if (itemsCabang.length > 0) {
          await apiClientCabang.delete(`/items(${itemsCabang[0].id})`, {
            headers: customHeaders
          })
        }
      } catch (error) {
        console.log(error)
      }
    }
  }

  const handlePageClick = (pageNumber: number) => {
    const startIndex = (pageNumber - 1) * itemsPerPage
    const endIndex = startIndex + itemsPerPage

    const itemsToDisplay = items.slice(startIndex, endIndex)

    setDisplayedItems(itemsToDisplay)
    setCurrentPage(pageNumber)
  }

  const handlePrevClick = () => {
    if (currentPage > 1) {
      setDisplayedItems(
        items.slice(
          (currentPage - 2) * itemsPerPage,
          (currentPage - 1) * itemsPerPage
        )
      )
      setCurrentPage(currentPage - 1)
    }
  }

  const handleNextClick = () => {
    if (currentPage < totalPages) {
      setDisplayedItems(
        items.slice(
          currentPage * itemsPerPage,
          (currentPage + 1) * itemsPerPage
        )
      )
      setCurrentPage(currentPage + 1)
    }
  }

  return (
    <div className='container mx-auto pl-6 pr-6'>
      <div className='flex justify-between items-center mt-[100px]'>
        <h2 className='text-2xl font-bold'>Pusat</h2>
        <button
          className='bg-blue-500 hover:bg-blue-600 text-white font-medium py-2 px-4 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-400'
          onClick={() => setIsModalOpen(true)}
        >
          Add Item
        </button>
      </div>
      <div className='relative overflow-x-auto shadow-md sm:rounded-lg mt-5'>
        <table className='w-full text-sm text-left text-gray-500'>
          <thead className='text-xs text-gray-700 uppercase bg-gray-50'>
            <tr>
              <th scope='col' className='px-6 py-3'>
                No.
              </th>
              <th scope='col' className='px-6 py-3'>
                Name
              </th>
              <th scope='col' className='px-6 py-3'>
                Type
              </th>
              <th scope='col' className='px-6 py-3'>
                Category
              </th>
              <th scope='col' className='px-6 py-3'>
                Unit of Measurement
              </th>
              <th scope='col' className='px-6 py-3'>
                <span className='sr-only'>Action</span>
              </th>
            </tr>
          </thead>
          <tbody>
            {isLoading ? (
              <tr>
                <td colSpan={6} className='text-center py-4'>
                  Loading...
                </td>
              </tr>
            ) : items && items.length === 0 ? (
              <tr>
                <td colSpan={6} className='text-center py-4'>
                  No items found.
                </td>
              </tr>
            ) : (
              displayedItems.map((item: any, index: any) => {
                return (
                  <tr
                    key={item.id}
                    className={
                      index !== items.length - 1
                        ? 'bg-white border-b hover:bg-gray-50'
                        : 'bg-white hover:bg-gray-50'
                    }
                  >
                    <th
                      scope='row'
                      className='px-6 py-4 font-medium text-gray-900 whitespace-nowrap '
                    >
                      {item.number}
                    </th>
                    <td className='px-6 py-4'>{item.displayName || '-'}</td>
                    <td className='px-6 py-4'>{item.type}</td>
                    <td className='px-6 py-4'>
                      {item.itemCategory ? item.itemCategory.description : '-'}
                    </td>
                    <td className='px-6 py-4'>
                      {item.baseUnitOfMeasure
                        ? item.baseUnitOfMeasure.displayName
                        : '-'}
                    </td>
                    <td className='px-6 py-4 flex justify-end'>
                      <span
                        onClick={() => {
                          handleEdit(item)
                        }}
                        className='cursor-pointer mr-5'
                      >
                        <Image src={Edit} alt='Edit' width={24} height={24} />
                      </span>
                      <span
                        onClick={() => {
                          handleDelete(item)
                        }}
                        className='cursor-pointer'
                      >
                        <Image
                          src={Delete}
                          alt='Delete'
                          width={20}
                          height={20}
                        />
                      </span>
                    </td>
                  </tr>
                )
              })
            )}
          </tbody>
        </table>
      </div>
      <div className='flex justify-center items-center mt-5 gap-3'>
        <button
          className='bg-blue-500 hover:bg-blue-600 text-white font-medium py-2 px-4 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-400'
          onClick={() => handlePrevClick()}
          disabled={currentPage === 1}
        >
          Prev
        </button>
        {getPageButtons()}
        <button
          className='bg-blue-500 hover:bg-blue-600 text-white font-medium py-2 px-4 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-400'
          onClick={() => handleNextClick()}
          disabled={currentPage === totalPages || totalPages === 0}
        >
          Next
        </button>
      </div>

      <Modal
        isOpen={isModalOpen}
        isEdit={isEdit}
        categories={categories}
        taxGroups={taxGroups}
        unitsOfMeasure={unitsOfMeasure}
        handleRevalidate={handleRevalidate}
        formEdit={formEdit}
        onClose={() => {
          setIsModalOpen(false)
          setIsEdit(false)
        }}
        closeModal={() => {
          setIsModalOpen(false)
          setIsEdit(false)
        }}
        backToFirstPage={() => {
          setCurrentPage(1)
          setDisplayedItems(items.slice(0, 10))
        }}
      />
      <ToastContainer />
    </div>
  )
}
