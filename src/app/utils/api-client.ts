import axios from 'axios'

export const apiClientCabang = axios.create({
  baseURL:
    'https://818b-104-28-250-133.ngrok-free.app/DynamicsNAV110/api/beta/companies(4129e342-8f2b-4c00-8f88-2a602aa61319)',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Basic ' + btoa('cabang:Testing123.'),
    ' ngrok-skip-browser-warning': true
  }
})

export const apiClientPusat = axios.create({
  baseURL:
    'https://f6f4-104-28-250-133.ngrok-free.app/DynamicsNAV110/api/beta/companies(24ab3e90-f9c9-418b-bf0a-9feea9464c85)',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Basic ' + btoa('pusat:Testing123.'),
    ' ngrok-skip-browser-warning': true
  }
})
